import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

export class UdpClient{
   port:number;
   Ip:string;
   CommunicationType:number[];
   CommunicationTypeAfterConvert:string[];
   constructor(port:number, ip:string, CommunicationName:number[]){
      this.port = port;
      this.Ip = ip;
      this.CommunicationType = CommunicationName;
      this.CommunicationTypeAfterConvert = [];
   }
   
}
export class MongoClient extends UdpClient{
   DataBaseName:string;
   CollectionName:string;
   constructor(port:number, ip:string, CommunicationName:number[], DataBaseName:string, CollectionName:string){
    super(port, ip, CommunicationName);
     this.DataBaseName = DataBaseName ;
     this.CollectionName =  CollectionName;
   }
 }
export class cardData{
   aircarftName:string;
   transmitionRate:number;
   type:number;
   constructor(name:string, type:number, rate:number){
      this.aircarftName = name;
      this.transmitionRate = rate;
      this.type = type;
   }
}
@Injectable({
   providedIn: 'root',
 })
export class NamesService{
   OnHomePage:Boolean = true;
    cardList:cardData[] = [];
    NameList:string[] = [];
    InDecodingList:string[] = [];
    MongoDBclientList:MongoClient[] = [];
    UdpClientList:UdpClient[] = [];
    public showcards:boolean = true;
    constructor(private _http:HttpClient){
      
   }
    editAircraftProperties(craftName:string, type:number, rate:number){
       if( this.cardList.some(item => item.aircarftName === craftName))
       {
         let ItemIndex = this.cardList.findIndex((item => item.aircarftName == craftName));
         this.cardList[ItemIndex].type = type;
         this.cardList[ItemIndex].transmitionRate = rate;
       }
       else
       {
          this.cardList.push(new cardData(craftName,type,rate));
       }
    }
    getTrnasmitionRate(craftName:string):number{
      if( this.cardList.some(item => item.aircarftName === craftName))
      {
         let ItemIndex = this.cardList.findIndex((item => item.aircarftName == craftName));
         return this.cardList[ItemIndex].transmitionRate;
      }
      return -1;
    }
    getCommunicationType(craftName:string):number{
      if( this.cardList.some(item => item.aircarftName === craftName))
      {
         let ItemIndex = this.cardList.findIndex((item => item.aircarftName == craftName));
         return this.cardList[ItemIndex].type;
      }
      return -1;
    }
    addCommunicationDecoding(name:string)
    {
       this.InDecodingList.push(name);
    }
    removeFromDecodingList(name:string)
    {
      const index = this.InDecodingList.indexOf(name, 0);
      if (index > -1) {
         this.InDecodingList.splice(index, 1);
      }
    }
    checkIfIsDeciding(name:string):boolean
    {
       console.log(name);
      for(let i=0; i<this.InDecodingList.length; i++)
      {
         if(this.InDecodingList[i] == name)
            return true;
      }
      return false;
    }
    AddMongoClient(client:MongoClient)
    {
       this.MongoDBclientList.push(client);
    }
    AddUdpClient(client:UdpClient){
       this.UdpClientList.push(client);
    }
    
}