import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NamesService } from '../names.service';
import { NavBarComponent } from '../nav-bar/nav-bar.component';
import { Location } from '@angular/common';


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})

export class CardComponent implements AfterViewInit  {
  @Input()name:string = " ";
  IsDecoding:boolean = false;
  IntervalId:any = 0;
  isLoading: boolean;
  constructor(private route:Router, private service:NamesService, private _http:HttpClient, private router:Router, private location: Location){
    this.route.routeReuseStrategy.shouldReuseRoute = () =>{
      return false;
    }
    this.isLoading = true;
  }
  ngAfterViewInit() {
    this.isLoading = false;
  }
  viewFrom(){
    this.route.navigateByUrl('/form/' +this.name);
  }
  ngOnInit(): void {
    this.IsDecoding = this.service.checkIfIsDeciding(this.name);
  }
  callpostRequest(){
    var responseCode;
    let transmitionRate = this.service.getTrnasmitionRate(this.name);
    let communicationType = this.service.getCommunicationType(this.name);
    if(transmitionRate > 0 && communicationType!=-1){
          this.service.addCommunicationDecoding(this.name);
          this._http.post<any>('https://localhost:44328/api/decodedIcd', { CommunicationType:this.name,DataDirection:communicationType,TransmissionRate:transmitionRate}).subscribe(response=>
          {
              responseCode = response.status;
          })
          this.IsDecoding = true;
          alert("decoded has started")
  }
  else{
    alert("decoded failed - fill aircraft form properties")
  }
}
callPostCancelRequest(){
  this._http.post<any>('https://localhost:44328/api/decodedIcd/CancelRequest', [this.name]).subscribe(response=>{
    this.IsDecoding = false; 
    this.service.removeFromDecodingList(this.name) 
    alert("decoded of: "+this.name+ " has been canceled")
  },(error) => {
   alert("you need to activate decoding before cancelling it!")
})
}
NavigateToProperties()
{
this.location.replaceState('/edit');
}
}
