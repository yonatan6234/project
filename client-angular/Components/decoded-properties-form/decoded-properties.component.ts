import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NamesService } from '../names.service';

@Component({
  selector: 'app-decoded-properties',
  templateUrl: './decoded-properties.component.html',
  styleUrls: ['./decoded-properties.component.css']
})
export class DecodedPropertiesComponent implements AfterViewInit{
  transitionRate:number = 0;
  selectedOption:string = " ";
  aircraftName:string = " ";
  isLoading:boolean; 
  constructor(private router:Router,private service:NamesService) {
    this.isLoading = true;
  }
  ngAfterViewInit() {
    this.isLoading = false;
  }
  saveData(){
    if(this.selectedOption == " " || this.transitionRate <= 0){
      alert("select communication type or invalid transmition rate");
    }
    else
    {
        let EnumNumberType:number = -1;
        if(this.selectedOption == "0")
            EnumNumberType = 0;
        else
            EnumNumberType = 1;
        this.service.editAircraftProperties(this.aircraftName,EnumNumberType,this.transitionRate);
        this.router.navigate(['/DecodedFrameProducer'])
        alert("properties for " + this.aircraftName +" saved successfuly")
    }
  }
  radioChangeHandler(event:any){
      this.selectedOption = event.target.value;
  }
  rateChange(event:any){
   this.transitionRate = event.traget.value
  }
  ngOnInit(): void {
    this.aircraftName = this.router.url.split('/')[3];
  }
  
}
