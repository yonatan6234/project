import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { NamesService } from '../names.service';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.css']
})
export class MainMenuComponent implements OnInit {
  todayDate:string;
  currentTime:string;
  constructor(private location:Location, private router:Router, public service:NamesService) { 
    var date =  new Date();
    let month = date.getMonth() +1;
    this.todayDate = date.getDate() + "/" + month + "/" +date.getFullYear();
    this.currentTime = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    setInterval(this.getCurrentTime, 1000);
  }
  ngOnInit(): void {
  }
  getCurrentTime(){
    var today = new Date();
    this.currentTime = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var item = document.getElementById("hour");
    if (item) item.innerHTML =this.currentTime;

  }
 NavToHomePage(){
   this.service.OnHomePage = true;
 }

}
