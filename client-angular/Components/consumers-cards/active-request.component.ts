import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MongoClient, NamesService, UdpClient } from '../names.service';
import { deccodedProperties } from '../user.modeul';


@Component({
  selector: 'app-active-request',
  templateUrl: './active-request.component.html',
  styleUrls: ['./active-request.component.css']
})

export class ActiveRequestComponent implements OnInit {
  UdpClien:UdpClient;
  MongoclientProps:MongoClient;
  requestsList:deccodedProperties[] = [];
  constructor(private _http:HttpClient, public service:NamesService) { 
    service.OnHomePage = false;
    this.UdpClien = new UdpClient(-1, "", []);
    this.MongoclientProps = new MongoClient(-1, "", [], "", "");
  }
  ngOnInit(): void {
  }
  portNumberChange(event:any){
    this.MongoclientProps.port = event.traget.value
   }
   UdpPortNumberChange(event:any)
   {
    this.UdpClien.port = event.traget.value;
   }
   IPAdressChange(event:any){
    this.MongoclientProps.Ip = event.traget.value;
   }
   UdpIPAdressChange(event:any){
      this.UdpClien.Ip = event.target.value;
    }
   DataBaseNameChange(event:any){
     this.MongoclientProps.DataBaseName = event.traget.value;
   }
   CollectionNameChange(event:any){
     this.MongoclientProps.CollectionName = event.traget.value;
   }
   CommunicationTypeChange(event:any){
    this.MongoclientProps.CommunicationType = event.traget.value;
   }
   startMongoClient(){
     if(this.MongoclientProps.Ip != ""  && this.MongoclientProps.port != -1 && this.MongoclientProps.CommunicationType.length > 0 && this.MongoclientProps.CollectionName != "" && this.MongoclientProps.DataBaseName != "") 
     {
      this._http.post<any>('https://localhost:44328/api/decodedIcd/ClientRequest', {Port: this.MongoclientProps.port,
      Ip: this.MongoclientProps.Ip,
      DataBaseName: this.MongoclientProps.DataBaseName,
      CollectionName:this.MongoclientProps.CollectionName,
      ConsumerTopic: this.MongoclientProps.CommunicationType}).subscribe(response=>
      {
        this.service.AddMongoClient(this.MongoclientProps);
        this.MongoclientProps.CommunicationTypeAfterConvert = this.convertFromEnum(this.MongoclientProps.CommunicationType);
        alert("MongoDB client has started")
      }, (error) => {                              //Error callback
        alert("error while creating MongoDB client")
      })
    }
    else{
      alert("Fill out missing porperties")
    }
   }
   startUdpClient(){
    if(this.UdpClien.Ip != " "  && this.UdpClien.port != -1 && this.UdpClien.CommunicationType.length  > 0 ) 
    {
     this._http.post<any>('https://localhost:44328/api/decodedIcd/UdpClientRequest', {Port: this.UdpClien.port,
     Ip: this.UdpClien.Ip,
     ConsumerTopic: this.UdpClien.CommunicationType}).subscribe(response=>
     {
       this.service.AddUdpClient(this.UdpClien);
       this.UdpClien.CommunicationTypeAfterConvert = this.convertFromEnum(this.UdpClien.CommunicationType);
       this.UdpClien = new UdpClient(-1, "", []);
      alert("UDP client has started")
     },
     (error) => {                              //Error callback
      alert("error while creating udp client")
    })
   }
   else{
     alert("Fill out missing porperties")
   }
  }
   MongoCommunicationTypeCheckBox(event:any) {
    if ( event.target.checked ) {
      this.MongoclientProps.CommunicationType.push(+event.target.value);
   }
   else
   {
    delete this.MongoclientProps.CommunicationType[event.target.value];
   }
}
UdpCommunicationTypeCheckBox(event:any) {
    if ( event.target.checked ) {
      this.UdpClien.CommunicationType.push(+event.target.value);
    }
    else
    {
        delete this.UdpClien.CommunicationType[event.target.value];
    }
}
clientPropertiesBtnClick(){
  this.service.showcards = false;
}
convertFromEnum(EnumString:number[]):string[]
{
  var communicationTypesAfterConvert:string[] = [];
  var communicationTypes:string[] = ["FlightBoxUp", "FlightBoxDown", "FiberBoxUp", "FiberBoxDown", "LandingBox" ];
  EnumString.forEach(element  => { 
    communicationTypesAfterConvert.push(communicationTypes[+element]);
  });
  return communicationTypesAfterConvert;
}
}
