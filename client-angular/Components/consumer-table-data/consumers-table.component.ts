import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MongoClient, NamesService, UdpClient } from '../names.service';

@Component({
  selector: 'app-consumers-table',
  templateUrl: './consumers-table.component.html',
  styleUrls: ['./consumers-table.component.css']
})
export class ConsumersTableComponent implements OnInit {
  MongoClients:MongoClient[];
  UdpClients:UdpClient[];
  constructor(public service:NamesService, private _http:HttpClient) { 
    this.MongoClients = this.service.MongoDBclientList;
    this.UdpClients = this.service.UdpClientList;
  }
  ngOnInit(): void {
  
  }
  showcards(){
     this.service.showcards = true;
  }
  cancelMongoConsumer(client:MongoClient){
    this._http.post<any>('https://localhost:44328/api/decodedIcd/CancelMongoClient', { Port: client.port,
    Ip: client.Ip,
    DataBaseName:client.DataBaseName,
    CollectionName:client.CollectionName,
    ConsumerTopic: client.CommunicationType}).subscribe(response=>
    {
      alert("MongoDB client canceled")
      this.deleteRowMongo(client);
    })
  }
  cancelUdpConsumer(client:UdpClient){
    this._http.post<any>('https://localhost:44328/api/decodedIcd/CancelUdpClient', { Port: client.port,
    Ip: client.Ip,
    ConsumerTopic: client.CommunicationType}).subscribe(response=>
    {
      alert("Udp client canceled")
      this.deleteRowUdp(client);
    })
  }
  deleteRowMongo(d:any){
    const index = this.MongoClients.indexOf(d);
    this.MongoClients.splice(index, 1);
}
deleteRowUdp(d:any){
  const index = this.UdpClients.indexOf(d);
  this.UdpClients.splice(index, 1);
}


}
