using Confluent.Kafka;
using HandleIcdLibrary;
using HandleKafkaLib.CosumersProperties;
using HandleKafkaLibrary;
using HandleKafkaLibrary.ClientConsumers;
using HandleKafkaLibrary.CosumersProperties;
using HandleKafkaLibrary.TopicsEnum;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace HandleKafkaLib
{
    public class KafkaConsumerCreator
    {
        private ConsumerConfig _config;
        public KafkaConsumerCreator()
        {
            _config = new ConsumerConfig
            {
                GroupId = "gid-consumers",
                BootstrapServers = "localhost:9092"
            };
        }
        public void CreateKafkaConsumerListener(KafkaTopicsEnum topic)
        {
            using (var consumer = new ConsumerBuilder<Null, string>(_config).Build())
            {
                KafkaConsumerManager consumerManager = KafkaConsumerManager.GetInstance();
                List<BasicClientConsumer> ConsumerClients = consumerManager.TopicConsumersDict[topic];
                string topicUplinkName = topic + "-Uplink";
                string topicDownlinkName = topic + "-Downlink";
                List<string> topics = new List<string>()
                { 
                    topicDownlinkName,
                    topicUplinkName
                };
                consumer.Subscribe(topics); //to listen both uplink and downlink kafka topics
                while (ConsumerClients.Count > 0)
                {
                    var data = consumer.Consume(); // read data from kafka
                    DecodedFrameDTO decodedFrame = JsonConvert.DeserializeObject<DecodedFrameDTO>(data.Value); //convert kafka data to decodedFrameObject
                    foreach (var consumerClient in ConsumerClients)
                    {
                        consumerClient.Consume(decodedFrame);
                    }
                }
            }
        }
    }
}
