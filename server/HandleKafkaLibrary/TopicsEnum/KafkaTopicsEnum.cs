﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HandleKafkaLibrary.TopicsEnum
{
    public enum KafkaTopicsEnum { FlightBoxUp, FlightBoxDown, FiberBoxUp, FiberBoxDown, LandingBox };
}
