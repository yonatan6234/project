using HandleKafkaLib;
using HandleKafkaLibrary.ClientConsumers;
using HandleKafkaLibrary.CosumersProperties;
using HandleKafkaLibrary.TopicsEnum;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace HandleKafkaLibrary
{
    public class KafkaConsumerManager
    {
        public Dictionary<KafkaTopicsEnum, List<BasicClientConsumer>> TopicConsumersDict { get; set; } 
        /// <summary>
        /// singleton
        /// </summary>
        private KafkaConsumerManager()
        {
            this.TopicConsumersDict = new Dictionary<KafkaTopicsEnum, List<BasicClientConsumer>>();
        }
        public static KafkaConsumerManager _instance = null;
        public static KafkaConsumerManager GetInstance()
        {
            _instance = _instance ?? new KafkaConsumerManager();
            return _instance;
        }
        public void AddConsumer(BasicClientConsumer clientProps)
        {
            foreach (var topic in clientProps.ConsumerProperties.ConsumerTopic)
            {
                if (this.TopicConsumersDict.ContainsKey(topic))
                {
                    // adding to topic consumer list
                    this.TopicConsumersDict[topic].Add(clientProps);
                    if (this.TopicConsumersDict[topic].Count == 1)
                    {
                        CreateConsumerListener(topic); //thread closed because there weren't any listeners...
                    }
                }
                else
                {
                    // creating new topic listener
                    List<BasicClientConsumer> clients = new List<BasicClientConsumer>() { clientProps };
                    this.TopicConsumersDict.Add(topic, new List<BasicClientConsumer>(clients));
                    CreateConsumerListener(topic); //creating kafka consumer thread
                }
            }
        }
        public void CreateConsumerListener(KafkaTopicsEnum topic)
        {
            KafkaConsumerCreator consumer = new KafkaConsumerCreator();
            Thread TopicListenter = new Thread(o => consumer.CreateKafkaConsumerListener(topic));
            TopicListenter.Start();
        }
    }
}
