﻿using Confluent.Kafka;
using HandleIcdLibrary;
using System;
using System.Collections.Generic;
using System.Text;

namespace HandleKafkaLibrary.CosumersProperties
{
    interface IconsumeData
    {
        public void Consume(DecodedFrameDTO decodedFrame);
        public bool Compare(BasicClientProperties properties);
    }
}
