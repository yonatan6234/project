﻿using HandleIcdLibrary;
using HandleKafkaLibrary.CosumersProperties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HandleKafkaLibrary.ClientConsumers
{
    /// <summary>
    /// Base class for consumers that consume data from kafkaConsumerManager
    /// </summary>
    public abstract class BasicClientConsumer : IconsumeData
    {
        public BasicClientProperties ConsumerProperties { get; set; }
        public BasicClientConsumer()
        {
            this.ConsumerProperties = null;
        }
        public BasicClientConsumer(BasicClientProperties consumerProperties)
        {
            this.ConsumerProperties = consumerProperties;
        }
        public abstract void Consume(DecodedFrameDTO decodedFrame);
        public virtual bool Compare(BasicClientProperties properties) 
        {
            return this.ConsumerProperties.Port == properties.Port && this.ConsumerProperties.Ip == properties.Ip && this.ConsumerProperties.ConsumerTopic.All(properties.ConsumerTopic.Contains);
        }
    }
}
