﻿using HandleIcdLibrary;
using HandleKafkaLibrary.CosumersProperties;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace HandleKafkaLibrary.ClientConsumers
{
    /// <summary>
    /// Udp client consumer - consume data from kafkaConsumerManager
    /// </summary>
    public class UdpClientConsumer : BasicClientConsumer
    {
        public UdpClient UDPclient { get; set; } 
        public UdpClientConsumer() : base()
        {
            this.UDPclient = null;
        }
        public UdpClientConsumer(BasicClientProperties udpProperties) : base(udpProperties)
        {
            if(base.ConsumerProperties is UdpClientProperties)
            {
                this.UDPclient = new UdpClient();
                IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(base.ConsumerProperties.Ip), base.ConsumerProperties.Port); // endpoint where Udp Client
                this.UDPclient.Connect(endPoint);
            }
        }
        public override void Consume(DecodedFrameDTO decodedFrame) 
        {
            if(base.ConsumerProperties is UdpClientProperties)
            {
                string decodedFrameJson = JsonConvert.SerializeObject(decodedFrame);
                byte[] data = Encoding.ASCII.GetBytes(decodedFrameJson);
                this.UDPclient.Send(data, data.Length);
            }
        }
        public override bool Compare(BasicClientProperties properties) 
        {
            return base.ConsumerProperties is UdpClientProperties && properties is UdpClientProperties && base.Compare(properties);
        }
    }
}
