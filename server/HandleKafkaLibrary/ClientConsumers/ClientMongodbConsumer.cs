﻿using HandleIcdLibrary;
using HandleKafkaLib.CosumersProperties;
using HandleKafkaLibrary.CosumersProperties;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace HandleKafkaLibrary.ClientConsumers
{
    /// <summary>
    /// mongodb client consumer - inserts data from kafkaConsumerManager to DB.
    /// </summary>
    public class ClientMongodbConsumer : BasicClientConsumer
    {
        public MongoClient ConsumerMongoClient { get; set; }
        public ClientMongodbConsumer() :base()
        {
            this.ConsumerMongoClient = null;
        }
        public ClientMongodbConsumer(BasicClientProperties consumerProperties) : base(consumerProperties)
        {
            if(base.ConsumerProperties is MongodbClientProperties)
            {
                string MongoClientConnectionString = "mongodb://" + base.ConsumerProperties.Ip + ":" + base.ConsumerProperties.Port;
                this.ConsumerMongoClient = new MongoClient(MongoClientConnectionString); //connecting to mongoDB 
            }
        }
        public override void Consume(DecodedFrameDTO decodedFrame)
        {
            if(base.ConsumerProperties is MongodbClientProperties)
            {
                MongodbClientProperties mongoProperties = (MongodbClientProperties)base.ConsumerProperties;
                var dataBase = this.ConsumerMongoClient.GetDatabase(mongoProperties.DataBaseName); //DB name
                var decodedFramesCollection = dataBase.GetCollection<BsonDocument>(mongoProperties.CollectionName); //collection Name
                var bsonDocument = decodedFrame.ToBsonDocument();
                decodedFramesCollection.InsertOne(bsonDocument);
            }
        }
        public override bool Compare(BasicClientProperties properties)
        {
            if (base.Compare(properties) && base.ConsumerProperties is MongodbClientProperties && properties is MongodbClientProperties)
            {
                MongodbClientProperties clientProperties = (MongodbClientProperties)base.ConsumerProperties;
                MongodbClientProperties otherClientProps = (MongodbClientProperties)properties;
                return clientProperties.DataBaseName == otherClientProps.DataBaseName && clientProperties.CollectionName == otherClientProps.CollectionName;
            }
            return false;
        }
    }
}
