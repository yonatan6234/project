using HandleIcdLibrary;
using HandleKafkaLibrary.TopicsEnum;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using TinyCsvParser.TypeConverter;

namespace HandleKafkaLibrary.CosumersProperties
{
    /// <summary>
    /// base class for client consumer properties
    /// </summary>
    [JsonConverter(typeof(BasicClientProperties))]
    public abstract class BasicClientProperties 
    {
        [Required(ErrorMessage = "port is required")]
        public int Port { get; set; }
        [Required(ErrorMessage = "Ip is required")]
        public string Ip { get; set; }
        [Required(ErrorMessage = "Consumer topic is required")]
        public List<KafkaTopicsEnum> ConsumerTopic { get; set; } 
        public BasicClientProperties()
        {
            this.Port = -1;
            this.Ip = "";
            this.ConsumerTopic = new List<KafkaTopicsEnum>();
        }
        public BasicClientProperties(int port, string ip, List<KafkaTopicsEnum> topic)
        {
            this.Port = port;
            this.Ip = ip;
            this.ConsumerTopic = topic;
        }
    }
}
