﻿using Confluent.Kafka;
using HandleIcdLibrary;
using HandleKafkaLibrary;
using HandleKafkaLibrary.CosumersProperties;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Threading;

namespace HandleKafkaLib
{
    public class KafkaProducer
    {
        private ProducerConfig _config;
        public KafkaProducer(ProducerConfig config)
        {
            this._config = config;
        }
        //singelton
        public static KafkaProducer _instance = null;
        public static KafkaProducer GetInstance(ProducerConfig config)
        {
            _instance = _instance ?? new KafkaProducer(config);
            return _instance;
        }
        public async void Write(string topic, DecodedFrameDTO frame)
        {
            string serializedDecodedFrame = JsonConvert.SerializeObject(frame);
            using (var producer = new ProducerBuilder<Null, string>(_config).Build())
            {
                await producer.ProduceAsync(topic, new Message<Null, string> { Value = serializedDecodedFrame });
                producer.Flush(TimeSpan.FromSeconds(10));
            }
        }
    }
}
