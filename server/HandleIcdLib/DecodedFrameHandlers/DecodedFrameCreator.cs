﻿using Confluent.Kafka;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace HandleIcdLibrary
{
    public class DecodedFrameCreator
    {
        private Random _rnd;
        private string _filePath;
        private List<DecodedItem> _frameDecodedItems;
        public DecodedFrameCreator(string path)
        {
            this._filePath = path;
            _rnd = new Random();
            _frameDecodedItems = new List<DecodedItem>();
        }
        public async Task<DecodedFrameDTO> CreateRandomFrameTask(ProducerConfig config, string topic)
        {
            DecodedFrameDTO frame = new DecodedFrameDTO();
            await Task.Run(() =>
            {
                Icdframe icdItems = new Icdframe(_filePath);
                Dictionary<int, IcdItem> icdDicionaryItems = icdItems._frameDict;
                foreach (var icdItem in icdDicionaryItems.Values)
                {

                    DecodedItem newDecodeItem = new DecodedItem(icdItem.Name, GetRndNumber(icdItem));
                    _frameDecodedItems.Add(newDecodeItem);
                }
                DateTime decodedDate = DateTime.Now;
                string fileName = Path.GetFileName(_filePath);
                frame = new DecodedFrameDTO(_frameDecodedItems, decodedDate, fileName);
            });
            return frame;
        }

        public int GetRndNumber(IcdItem icdItem)
        {
            return _rnd.Next(icdItem.MinValue, icdItem.MaxValue);
        }

    }
}
