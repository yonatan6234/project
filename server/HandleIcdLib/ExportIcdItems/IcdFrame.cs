﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HandleIcdLibrary
{
    public class Icdframe
    {
        public Dictionary<int, IcdItem> _frameDict;
        public Icdframe(string filePath)
        {
            IcdDictionaryBuilder dictionaryBuilder = new IcdDictionaryBuilder(filePath);
            this._frameDict = new Dictionary<int, IcdItem>(dictionaryBuilder.InsertValues());
        }
        public override string ToString()
        {
            string data = "";
            foreach (var item in _frameDict.Values)
            {
                data += item.ToString();
            }
            return data;
        }
    }
}
