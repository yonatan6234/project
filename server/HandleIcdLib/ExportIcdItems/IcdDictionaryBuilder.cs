﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HandleIcdLibrary
{
    public class IcdDictionaryBuilder
    {
        /// <summary>
        /// builds Icd Items Dictionary
        /// </summary>
        private string _filePath;
        public IcdDictionaryBuilder(string path)
        {
            this._filePath = path;
        }
        public Dictionary<int, IcdItem> InsertValues()
        {
            Dictionary<int, IcdItem> icdItemsDictionary = new Dictionary<int, IcdItem>();
            ExportIcdItem ItemsExported = ExportIcdItem.GetInstance();
            string[] fileLines = File.ReadAllLines(_filePath);
            for (int i = 1; i < fileLines.Length; i++)
            {
                IcdItem curretnItem = ItemsExported.ExportFromLine(fileLines[i]);
                int id = ItemsExported.ExportId(fileLines[i]);
                icdItemsDictionary.Add(id, curretnItem);
            }
            return icdItemsDictionary;
        }
    }
}
